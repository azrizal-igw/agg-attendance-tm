var express = require('express');
var path = require('path');
var fs = require('fs');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');





// additional require
var cors = require('cors');

var index = require('./routes/index');
var users = require('./routes/users');






//----------------------------------------------------------//
//
// Loading Routes
//
//----------------------------------------------------------//

var cmsCimsDataSendingR = require('./routes/jobs/sendCmsDataToCims');






//----------------------------------------------------------//
//
// Application Startup and configuration
//
//----------------------------------------------------------//

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));

// app.set('view engine', '-c');
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
// app.use(logger('dev'));






//----------------------------------------------------------//
//
// Common middleware
//
//----------------------------------------------------------//

var logFile = fs.createWriteStream('./myLogFile.log', {flags: 'a'}); //use {flags: 'w'} to open in write mode
app.use(logger("combined", {stream: logFile}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  limit: '50mb',
  extended: true,
  parameterLimit:50000
}));
app.use(cookieParser());

// cors enabled
app.use(cors());

// static files
app.use(express.static(path.join(__dirname, 'public')));







//----------------------------------------------------------//
//
// Initializing Routes
//
//----------------------------------------------------------//

app.use('/', index);
app.use('/users', users);
app.use('/jobs', cmsCimsDataSendingR);







//----------------------------------------------------------//
//
// Error handler
//
//----------------------------------------------------------//

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {

  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  // res.render('error');
  res.json(err)
});






module.exports = app;
