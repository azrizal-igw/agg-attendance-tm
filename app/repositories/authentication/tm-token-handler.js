var cfg = require('config');
var fs = require('fs-extra');
var async = require('async');
var moment = require('moment');
var _ = require('lodash');

// services
var tmService = require('../../services/Tm');

var TmTokenHandler = function(){

	this.getStoredAccessToken = function(callback){

		// we going to request for new token everytime
		tmService.login().then(
			function(result){
				callback(null, result.data.Guid)
			},
			function(error){
				callback(error)
			}
		)	
	}

};

module.exports = (function(){

	var instance = new TmTokenHandler();

	return instance;

})();