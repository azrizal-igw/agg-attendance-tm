var TrainingCourseModel = require("../../../models/Staffdetail")
var async = require('async')
var _ = require('lodash')
var q = require('q')
var tmApiService = require('../../../services/Tm')
var appHelper = require('../../../utilities/Helper')

var JobCimsRepo = function(){

	this.sendTrainingCourseData = function(callback, tmToken){
		var start = appHelper.clock()
		// send a package of 10 pages of data everytime
		var limit = 500;
		var page = 1;
		var processingCourse = []
		var processingCourseLength = 0;
		var failedCourses = []
		var checkCompletion = function(cb){
			--processingCourseLength
			if(!processingCourseLength){
				if(failedCourses.length){				
					processingCourse = failedCourses
					processingCourseLength = processingCourse.length
					failedCourses = []
					sendData(cb)					
				}
				else{
					var duration = appHelper.clock(start)
					console.log("=====================================================")
					console.log("Current Page: "+ page)
					console.log("Processing Courses tooks: "+ duration + " ms.")
					console.log("=====================================================")					
					page++
					processData();
				}
			}
		}

		var collectData = function(cb){			
			TrainingCourseModel.getNotSentRecords(limit, page).then(
				function(courses){
					if(Object.keys(courses).length){
						processingCourse = courses
						processingCourseLength = processingCourse.length	
					}		
					cb(null, courses)												
				},
				function(error){
					cb(error)
				}
			)			
		}

		var sendData = function(cb){			
			// send request to axios
			_.each(processingCourse, function(course){
				tmApiService.updateTrainingCourseData2(course, tmToken).then(
					function(result){
						// console.log("Success: "+ course.TRAINING_ID);
						console.log(result.data)

						// update sent status if theres no error whatsoever
						if(! (typeof result.data.Error !== 'undefined') ){
							TrainingCourseModel.updateSendCimsSuccess(course)

						} else {
							TrainingCourseModel.updateSendLogCimsError(course, result)
						}						
						checkCompletion(cb)
					},
					function(error){
						console.log("Failed: "+ course.TRAINING_ID)
						//console.log(error)
						//process.exit()
						failedCourses.push(course)
						checkCompletion(cb)
					}
				)
			})
		}

		var processData = function(){
			console.log("Page: "+page)
			async.waterfall([
				function(cb){
					// get data from cms
					collectData(cb)
				},
				function(courses, cb){
					if(Object.keys(courses).length){
						sendData(cb)	
					}
					else{
						cb(null, "Done.")
					}
				}
			], function(error, result){
				if(error) callback(error)
				else callback(null, result)
			})
		} // end processData()
		
		// initialization
		processData()


	}

};




module.exports = (function(){
	var instance = new JobCimsRepo();
	return instance;
})();

