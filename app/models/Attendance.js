var Sequelize = require('sequelize');
var q = require('q')
var moment = require('moment');
var datetime = require('node-datetime');
// var moment = require('moment-timezone');





var MemberDetailModel = function(){

	// var sequelize = new Sequelize('agg', 'root', 'Msd!mysql!pass!383', {
	var sequelize = new Sequelize('agg', 'root', 'msd!db!383', {
	// var sequelize = new Sequelize('agg', 'root', '', {
		// host: '10.10.153.55',
		host: '10.10.153.15',
		// host: 'localhost',
		dialect: 'mysql',
		timezone: '+08:00',
		pool: {
			max: 5,
			min: 0,
			idle: 10000
		},
	});
	




	var MemberDetail = sequelize.define('member_detail', {
		PI1M_REFID: { type: Sequelize.STRING },
		SERVICE_PROVIDER: { type: Sequelize.STRING },
		Staff_IC: { type: Sequelize.STRING },
		CheckIn_Date: { type: Sequelize.DATE },
		CheckIn_Time: { type: Sequelize.DATE },
		CheckOut_Time: { type: Sequelize.DATE },
		CheckIn_Status: { type: Sequelize.STRING },
		Date: { type: Sequelize.DATE },
		Send_Date: { type: Sequelize.DATE },
		Error_Message: { type: Sequelize.STRING },
	}, {
		timestamps: false,
		freezeTableName: true,
		tableName: 'mycommStaffAttendance',
		comment: "Staff Attendance"
	});




	this.getTotalNotSentRecords = function(){
		var deferred = q.defer()
		// MemberDetail.findAndCountAll({where:{Send_Date:null,Error_Message:null, }}).then(
		MemberDetail.findAndCountAll({where:{Send_Date:null,Error_Message:null,SERVICE_PROVIDER:'TM', }}).then(
			function(result){
				deferred.resolve(result.count)
			},
			function(error){
				deferred.reject(error)
			}
		);	
		return deferred.promise
	}




	this.getNotSentRecords = function(limit, page){
		var deferred = q.defer()		
		MemberDetail.findAll({
			attributes: [
				'id',
				'PI1M_REFID',
				'SERVICE_PROVIDER',
				'Staff_IC',
				'CheckIn_Date',
				'CheckIn_Time',
				'CheckOut_Time',
				'CheckIn_Status',
				'Date',
				'Send_Date',
				'Error_Message',
				[sequelize.fn('date_format', sequelize.col('CheckIn_Time'), '%d-%m-%Y %H:%i:%s'), 'CheckIn_Time'],				
				[sequelize.fn('date_format', sequelize.col('CheckOut_Time'), '%d-%m-%Y %H:%i:%s'), 'CheckOut_Time'],				
				[sequelize.fn('date_format', sequelize.col('CheckIn_Date'), '%d-%m-%Y'), 'CheckIn_Date'],				
			],
			// where: {Send_Date: null, Error_Message: null, },
			where: {Send_Date: null, Error_Message: null, SERVICE_PROVIDER: 'TM', },
			// where: {Error_Message: null, },
			// where: {Date: {$like: '%2017%'},},
			//where: {SENT: 0, PI1M_REFID: {$like: 'Q0%'},},
			limit: limit,
			order: 'id DESC',
			offset: (page - 1) * limit
		}).then(
			function(members){
				deferred.resolve(members)
			},
			function(err){
				deferred.reject(err)
			}
		)
		return deferred.promise
	}

	this.updateSendCimsSuccess = function(member){
		try{

			// var dt = datetime.create()
			// var now = dt.format('Y-m-d H:M:S')	


			var deferred = q.defer()
			member.update({
				// Send_Date: now,
				Send_Date: moment().format()
			}).then(
				function(res){
					deferred.resolve(res)
				},
				function(err){
					deferred.reject(err)
					console.log(err);
				}
			);	
		}
		catch(error){
			deferred.reject(error)
		}
		return deferred.promise
	}

	this.updateSendLogCimsError = function(member, response){
		try{
			var deferred = q.defer()
			member.update({
				Error_Message: response.data['Error']
			}).then(
				function(res){
					deferred.resolve(res)
				},
				function(err){
					console.log("*******************************************")
					console.log("*******************************************")
					console.log("Error Failed To Logged")
					console.log("*******************************************")
					console.log("*******************************************")
					process.exit(1)
					deferred.reject(err)
				}
			);	
		}
		catch(error){
			deferred.reject(error)
		}
		return deferred.promise
	}

};













module.exports = (function(){
	var instance = new MemberDetailModel();
	return instance
})();

