var tmTokenVerifyMiddleware = require('./tmTokenVerify');

var Middleware = function(){
		
	this.tmRequestPrep = [
		tmTokenVerifyMiddleware.validateToken
	];

};

module.exports = (function(){

	var instance = new Middleware();

	return instance;

})();