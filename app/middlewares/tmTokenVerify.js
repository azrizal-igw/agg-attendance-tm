var async = require('async');
var moment = require('moment');
var q = require('q');

// repositories
var tmTokenHandlerRepo = require('../repositories/authentication/tm-token-handler');

var TmTokenverify = function(){
	
	this.validateToken = function(req, res, next){

		async.waterfall([
				function(callback){
					tmTokenHandlerRepo.getStoredAccessToken(callback)
				}
			], function(error, result){				
				req.tmCredentials = result
				next()								
			})

	}
};

module.exports = (function(){

	var instance = new TmTokenverify();

	return instance;

})();