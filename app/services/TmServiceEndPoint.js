var cfg = require('config')
var moment = require('moment')
// moment.tz.setDefault("Asia/Kuala_Lumpur");

var TmServiceEndPoint = function(){	



	
	this.login = function(){		
		var base64Data = new Buffer("login?user[UserName]="+
		cfg.tm.username+"&user[Password]="+
		cfg.tm.password).toString('base64')		
		return cfg.tm.baseUrl + '/HttpAuthentication/?' + base64Data
	}




	// upsert staff attendance
	// -----------------------
	this.updateTrainingCourseData = function(value, token) {
		var str = "upsert/mcmccims/usp_im.v_pi1m_cms_staff_att:checkin_time,checkout_time,checkin_status;to_date('"+
		value.CheckIn_Time+"','dd-mm-yyyy hh24:MI:ss'),to_date('"+
		value.CheckOut_Time+"','dd-mm-yyyy hh24:MI:ss'),'"+
		value.CheckIn_Status+"'?pi1m_refid='"+
		value.PI1M_REFID.trim()+"'&&&service_provider='TM'&&&staff_ic='"+
		value.Staff_IC.trim()+"'&&&checkin_date=to_date('"+
		value.CheckIn_Date+"','dd-mm-yyyy')&&&$token='"+
		token+"'";
		// console.log(str);
		var base64Data = new Buffer(str).toString('base64');
		return cfg.tm.baseUrl + '/data/?' + base64Data
	}




	// // upsert staff detail
	// // -------------------
	// this.updateTrainingCourseData2 = function(value, token) {
	//     var str = "upsert/mcmccims/usp_im.v_pi1m_cms_staff_details:staff_name,contact_number,contact_email,position,status;\""+
	//     value.Staff_Name.trim()+"\",'"+
	//     value.Contact_Number.trim()+"',\""+
	//     value.Contact_Email.trim()+"\",'"+
	//     value.Position.trim()+"','"+
	//     value.Status.trim()+"'?pi1m_refid='"+
	//     value.PI1M_REFID.trim()+"'&&&service_provider='TM'&&&staff_ic='"+
	//     value.Staff_IC.trim()+"'&&&$token='"+
	//     token+"'";
	// 	// console.log(str);	    
	// 	var base64Data = new Buffer(str).toString('base64');
	// 	return cfg.tm.baseUrl + '/data/?' + base64Data
	// }




	// upsert staff detail
	// -------------------
	this.updateTrainingCourseData2 = function(value, token) {
		var str = "upsert/mcmccims/usp_im.v_pi1m_cms_staff_details:pi1m_refid,staff_name,contact_number,contact_email,position,status;\""+
	    value.PI1M_REFID.trim()+"\",\""+
	    value.Staff_Name.trim()+"\",\""+
	    value.Contact_Number.trim()+"\",\""+
	    value.Contact_Email.trim()+"\",\""+
	    value.Position.trim()+"\",\""+
	    value.Status.trim()+"\"?service_provider=\"TM\"&&&staff_ic=\""+
	    value.Staff_IC.trim()+"\"&&&$token=\""+
	    token+"\"";
		// console.log(str);	    
		var base64Data = new Buffer(str).toString('base64');
		return cfg.tm.baseUrl + '/data/?' + base64Data
	}




	// // upsert staff attendance using post method
	// // -----------------------------------------
	// this.postStaffAttendance = function(value, token) {
	// 	// var str = "upsert/mcmccims/usp_im.v_pi1m_cms_member_detail:member_id,member_name,gender,birthdate,race,occupation,oku_status,member_status,last_login,pi1m_refid,service_provider,member_ic;service_provider,member_ic?$token='"+token+"'";
	// 	var str = "upsert/mcmccims/usp_im.v_pi1m_cms_staff_att:pi1m_refid,service_provider,staff_ic,checkin_date,checkin_time,checkout_time,checkin_status;pi1m_refid,service_provider,staff_ic,checkin_date?$token='"+token+"'";
	// 	var base64Data = new Buffer(str).toString('base64');		
	// 	return cfg.tm.baseUrl + '/data/?' + base64Data
	// }




	// // upsert staff detail using post method
	// // -------------------------------------
	// this.postStaffDetail = function(value, token) {
	// 	// var str = "upsert/mcmccims/usp_im.v_pi1m_cms_member_detail:member_id,member_name,gender,birthdate,race,occupation,oku_status,member_status,last_login,pi1m_refid,service_provider,member_ic;service_provider,member_ic?$token='"+token+"'";
	// 	var str = "upsert/mcmccims/usp_im.v_pi1m_cms_staff_details:pi1m_refid,service_provider,staff_ic,staff_name,contact_number,contact_email,position,status;pi1m_refid,service_provider,staff_ic?$token='"+token+"'";
	// 	var base64Data = new Buffer(str).toString('base64');		
	// 	return cfg.tm.baseUrl + '/data/?' + base64Data
	// }





};




module.exports = (function(){
	var instance = new TmServiceEndPoint();
	return instance;
})();


