var cfg = require('config');
var axios = require('axios');
var q = require('q');
var tmServiceEndPoint = require('./TmServiceEndPoint');

var TmApiService = function(){

	this.login = function(){
		var deferred = q.defer()
		var url = tmServiceEndPoint.login()
		axios.get(url).then(
			function(result){
				deferred.resolve(result)
			},
			function(error){
				deferred.reject(error)
			}
		)
		return deferred.promise
	}

	this.updateTrainingCourseData = function(course, token){		
		var deferred = q.defer()
		var url = tmServiceEndPoint.updateTrainingCourseData(course, token)		
		axios.get(url).then(
			function(result){
				deferred.resolve(result)
			},
			function(error){
				deferred.reject(error)
			}
		)
		return deferred.promise
	}


	this.updateTrainingCourseData2 = function(course, token){		
		var deferred = q.defer()
		var url = tmServiceEndPoint.updateTrainingCourseData2(course, token)		
		axios.get(url).then(
			function(result){
				deferred.resolve(result)
			},
			function(error){
				deferred.reject(error)
			}
		)
		return deferred.promise
	}

};


module.exports = (function(){
	var instance = new TmApiService();
	return instance;
})();



