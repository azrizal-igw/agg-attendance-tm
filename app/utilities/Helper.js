var q = require('q')
var AppHelper = function(){

	this.clock = function(start) {
	    if ( !start ) return process.hrtime();
	    var end = process.hrtime(start);
	    return Math.round((end[0]*1000) + (end[1]/1000000));
	}

	this.testNetwork = function(){
		var deferred = q.defer()
		require('dns').resolve('www.google.com', function(err) {
			if (err) {				
				deferred.reject(err)
			} else {				
				deferred.resolve(true)
			}
		});
		return deferred.promise
	}


};

module.exports = (function(){
	var instance = new AppHelper();
	return instance;
})();