var express = require('express');
var router = express.Router();

// libs
var async = require('async');

// middleware
var tmRequestMiddleware = require('../../app/middlewares').tmRequestPrep;



// repos
var attendanceRepo = require('../../app/repositories/attendance');
var jobCimsTrainingCourseRepo = require('../../app/repositories/jobs/cims/attendance');



// repos
var attendanceRepo2 = require('../../app/repositories/staffdetail');
var jobCimsTrainingCourseRepo2 = require('../../app/repositories/jobs/cims/staffdetail');




/* GET home page. */
// router.get('/attendance', function(req, res) {
router.get('/attendance', [tmRequestMiddleware], function(req, res) {

	res.json("Process running on background");
	var process = function(){
		async.waterfall([			
			function(callback){
				jobCimsTrainingCourseRepo.sendTrainingCourseData(callback, req.tmCredentials)
			}
		], function(error, result){
			if(error) console.log(error)
			else {
				checkProcessAndRun()
			}
		})	
	}



	var checkProcessAndRun = function(){
		async.waterfall([
			function(callback){
				attendanceRepo.getTotalNotSentRecords(callback)
			}
		], function(error, result){
			if(error) console.log(error)
			else{
				if(result){
					process()
				}
				else{
					console.log("==================================")
					console.log("Done.")
					console.log("==================================")
				}
			}
		})
		
	}

	checkProcessAndRun()

});






router.get('/staffdetail', [tmRequestMiddleware], function(req, res) {

	res.json("Process running on background");
	var process = function(){
		async.waterfall([			
			function(callback){
				jobCimsTrainingCourseRepo2.sendTrainingCourseData(callback, req.tmCredentials)
			}
		], function(error, result){
			if(error) console.log(error)
			else {
				checkProcessAndRun()
			}
		})	
	}

	var checkProcessAndRun = function(){
		async.waterfall([
			function(callback){
				attendanceRepo2.getTotalNotSentRecords(callback)
			}
		], function(error, result){
			if(error) console.log(error)
			else{
				if(result){
					process()
				}
				else{
					console.log("==================================")
					console.log("Done.")
					console.log("==================================")
				}
			}
		})
		
	}

	checkProcessAndRun()

});





/* Staff Attendance Method Post */
router.get('/attendance/post', [tmRequestMiddleware], function(req, res) {

	res.json("Process running on background");

	async.waterfall([		
		function(callback){				
			jobCimsTrainingAttendeePostRepo.sendDataProcess(callback, req.tmCredentials)
		}
	], function(error, result){
		if (error) console.log(error)
		else console.log(result)
	})
});





module.exports = router;
